import { useContext, useEffect, useState } from "react";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import "./App.css";
import useToken from "./hooks/useToken";
import Home from "./pages/Home";
import LoginPage from "./pages/LoginPage";
import Profile from "./pages/Profile";
import Users from "./pages/Users";
import { AuthContext } from "./providers/AuthContextProvider";

const routesIsNotLoggedIn = (
  <>
    <Route index element={<Home />} />
    <Route path="/authentication" element={<LoginPage />} />
  </>
);

const routesIsLoggedIn = (
  <>
    <Route index element={<Home />} />
    <Route path="/users" element={<Users />} />
    <Route path="/profile" element={<Profile />} />
  </>
);

function App() {
  const [routes, setRoutes] = useState(routesIsNotLoggedIn);
  const { tokenProfileInfos, token } = useToken();
  const authContext = useContext(AuthContext);

  const getCurrentTokenProfile = () => {
    if (token) {
      authContext.dispatch({
        type: "LOGIN",
        payload: {
          isLoggedIn: true,
          currentUser: tokenProfileInfos(),
        },
      });
    }
  };
  useEffect(() => {
    getCurrentTokenProfile();
    if (authContext.state.isLoggedIn) {
      setRoutes(routesIsLoggedIn);
    } else if (!authContext.state.isLoggedIn) {
      setRoutes(routesIsNotLoggedIn);
    }
  }, [authContext.state.isLoggedIn]);

  return (
    <Router>
      <Routes>{routes}</Routes>
    </Router>
  );
}

export default App;
