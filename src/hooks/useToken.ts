import { useState } from "react";
import jwt from "jwt-decode";

export default function useToken() {
  const removeToken = () => {
    localStorage.removeItem("access_token");
  };

  const getToken = () => {
    const thisToken = localStorage.getItem("access_token");
    if (thisToken) {
      try {
        const { exp }: any = jwt(thisToken);
        const startTime: any = new Date(exp * 1000);
        const currentTime: any = new Date();
        if (startTime - currentTime <= 0) {
          removeToken();
          return undefined;
        }
      } catch (e) {
        removeToken();
        return undefined;
      }
    }

    return thisToken;
  };

  const getRole = () => {
    const accessToken = getToken();
    if (!accessToken) {
      return undefined;
    }
    const { userData }: any = jwt(accessToken);
    const { userType } = userData;
    return userType;
  };

  const getProfileInfos = () => {
    const accessToken = getToken();
    if (!accessToken) {
      return undefined;
    }
    const res = jwt(accessToken);
    return res;
  };

  const [token, setToken] = useState(getToken());

  const saveToken = (userToken: string | null) => {
    if (userToken) {
      localStorage.setItem("access_token", userToken);
      setToken(userToken);
    }
  };

  return {
    setToken: saveToken,
    token,
    tokenRole: getRole,
    tokenProfileInfos: getProfileInfos,
    tokenClear: removeToken,
  };
}
