import { useCallback, useContext, useState } from "react";
import useToken from "./useToken";
import Toaster from "../components/atoms/Toaster/Toaster";
import { render } from "@testing-library/react";
import { ApiService } from "../api/ApiService";
import { LoginDto } from "../types/login-dto";
import { AuthContext } from "../providers/AuthContextProvider";
import { useNavigate } from "react-router-dom";
import { User } from "../types/user";
import { PaginateUser } from "../types/paginate-user";
import { UserDto } from "../types/user-dto";
import { UpdateUserDto } from "../types/update-user-dto";

export default function useHandleUsers() {
  const [userList, setUserList] = useState<PaginateUser>();
  const [user, setUser] = useState<User>();
  const [page, setPage] = useState<number>(0);
  const [limit, setLimit] = useState<number>(20);
  const [maxPage, setMaxPage] = useState<number>(0);

  // useToken methods
  const { token, setToken, tokenClear, tokenProfileInfos } = useToken();
  const authContext = useContext(AuthContext);
  const navigate = useNavigate();

  const loginApp = useCallback(async (credentials: LoginDto) => {
    try {
      const { data } = await ApiService.login(credentials);
      authContext.state.login(data.token);
      authContext.dispatch({
        type: "LOGIN",
        payload: {
          isLoggedIn: true,
          currentUser: tokenProfileInfos(),
        },
      });
      render(<Toaster success toastMsg="Connexion réussie !" />);
      navigate("/");
    } catch (error) {
      render(<Toaster error toastMsg="La connexion a échouée !" />);
    }
  }, []);

  const getListUsers = useCallback(async () => {
    if (!token) {
      render(
        <Toaster
          error
          toastMsg="Vous devez être connecté afin de réaliser cette opération"
        />
      );
      return;
    }
    try {
      const { data } = await ApiService.listAllUser(token, {
        limit,
        page,
      });
      setUserList({
        users: data.data.users,
        pageNumber: parseInt(data.data.pageNumber),
        usersPerPage: parseInt(data.data.usersPerPage),
        usersCount: data.data.usersCount,
      });
      setMaxPage(Math.floor(data.data.usersCount / limit) ?? 0);
    } catch (error) {
      render(
        <Toaster
          error
          toastMsg="La récupération des utilisateurs a échouée !"
        />
      );
    }
  }, [page, maxPage]);

  const getUser = useCallback(async (userId: string) => {
    if (!token) {
      render(
        <Toaster
          error
          toastMsg="Vous devez être connecté afin de réaliser cette opération"
        />
      );
      return;
    }
    try {
      const { data } = await ApiService.getOneUser(token, userId);
      setUser(data.data);
    } catch (error) {
      render(
        <Toaster
          error
          toastMsg="La récupération de l'utilisateur a échouée !"
        />
      );
    }
  }, []);

  const createUser = useCallback(async (user: UserDto) => {
    if (!token) {
      render(
        <Toaster
          error
          toastMsg="Vous devez être connecté afin de réaliser cette opération"
        />
      );
      return;
    }
    try {
      await ApiService.createUser(token, user);
      render(
        <Toaster success toastMsg="La création de l'utilisateur a réussie !" />
      );
      getListUsers();
    } catch (error) {
      render(
        <Toaster error toastMsg="La création de l'utilisateur a échouée !" />
      );
    }
  }, []);

  const updateUser = useCallback(async (id: string, user: UpdateUserDto) => {
    if (!token) {
      render(
        <Toaster
          error
          toastMsg="Vous devez être connecté afin de réaliser cette opération"
        />
      );
      return;
    }
    try {
      await ApiService.updateUser(token, id, user);
      render(
        <Toaster
          success
          toastMsg="La modification de l'utilisateur a réussie !"
        />
      );
      getListUsers();
    } catch (error) {
      render(
        <Toaster
          error
          toastMsg="La modification de l'utilisateur a échouée !"
        />
      );
    }
  }, []);

  const deleteUser = useCallback(async (id: string) => {
    if (!token) {
      render(
        <Toaster
          error
          toastMsg="Vous devez être connecté afin de réaliser cette opération"
        />
      );
      return;
    }
    try {
      await ApiService.deleteUser(token, id);
      render(
        <Toaster
          success
          toastMsg="La suppression de l'utilisateur a réussie !"
        />
      );
      getListUsers();
    } catch (error) {
      render(
        <Toaster error toastMsg="La suppression de l'utilisateur a échouée !" />
      );
    }
  }, []);

  const nextPage = useCallback(async () => {
    if (!userList) return;
    if (page >= maxPage) {
      setPage(0);
      return;
    }
    setPage(page + 1);
  }, [page, maxPage]);

  const prevPage = useCallback(async () => {
    if (!userList) return;
    if (page <= 0) {
      setPage(maxPage);
      return;
    }
    setPage(page - 1);
  }, [page]);

  return {
    loginApp,
    userList,
    getListUsers,
    getUser,
    user,
    createUser,
    updateUser,
    deleteUser,
    nextPage,
    prevPage,
    page,
    maxPage,
  };
}
