import { render } from "@testing-library/react";
import axios from "axios";
import Toaster from "../components/atoms/Toaster/Toaster";
import { LoginDto } from "../types/login-dto";
import { configEnv } from "../environments";
import { OptionsDto } from "../types/options-dto";
import { UserDto } from "../types/user-dto";
import { UpdateUserDto } from "../types/update-user-dto";

export const UserService = {
  async login(credentials: LoginDto) {
    try {
      const res = await axios.post(
        `${configEnv.API_URL}/v1/login`,
        credentials
      );
      render(<Toaster success toastMsg="Connexion réussie !" />);
      return res.data;
    } catch (error) {
      render(<Toaster error toastMsg="La connexion a échouée !" />);
    }
  },
  async listAllUser(token: string, options?: OptionsDto) {
    const reqOptions = options ?? { limit: 100, page: 0 };
    const config = {
      headers: {
        "x-access-token": `${token}`,
      },
    };
    try {
      const res = await axios.get(
        `${configEnv.API_URL}/${configEnv.USERS_URL}?limit=${reqOptions.limit}&page=${reqOptions.page}`,
        config
      );
      return res.data;
    } catch (error) {
      render(
        <Toaster
          error
          toastMsg="La récupération des utilisateurs a échouée !"
        />
      );
    }
  },
  async createUser(token: string, user: UserDto) {
    const config = {
      headers: {
        "x-access-token": `${token}`,
      },
    };
    try {
      const res = await axios.post(
        `${configEnv.API_URL}/${configEnv.USERS_URL}`,
        user,
        config
      );
      return res.data;
    } catch (error) {
      render(
        <Toaster error toastMsg="La création de l'utilisateur a échouée !" />
      );
    }
  },
  async getOneUser(token: string, id: number) {
    const config = {
      headers: {
        "x-access-token": `${token}`,
      },
    };
    try {
      const res = await axios.get(
        `${configEnv.API_URL}/${configEnv.USERS_URL}/${id}`,
        config
      );
      return res.data;
    } catch (error) {
      render(
        <Toaster
          error
          toastMsg="La récupération de l'utilisateur a échouée !"
        />
      );
    }
  },
  async updateUser(token: string, id: number, user: UpdateUserDto) {
    const config = {
      headers: {
        "x-access-token": `${token}`,
      },
    };
    try {
      const res = await axios.put(
        `${configEnv.API_URL}/${configEnv.USERS_URL}/${id}`,
        user,
        config
      );
      <Toaster
        success
        toastMsg="La modification de l'utilisateur a réussie !"
      />;
      return res.data;
    } catch (error) {
      render(
        <Toaster
          error
          toastMsg="La modification de l'utilisateur a échouée !"
        />
      );
    }
  },
  async deleteUser(token: string, id: number) {
    const config = {
      headers: {
        "x-access-token": `${token}`,
      },
    };
    try {
      const res = await axios.delete(
        `${configEnv.API_URL}/${configEnv.USERS_URL}/${id}`,
        config
      );
      <Toaster
        success
        toastMsg={`La suppression de l'utilisateur ${id} a réussie !`}
      />;
      return res.data;
    } catch (error) {
      render(
        <Toaster error toastMsg="La suppression de l'utilisateur a échouée !" />
      );
    }
  },
};
