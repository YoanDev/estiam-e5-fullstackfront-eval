import axios from "axios";
import { LoginDto } from "../types/login-dto";
import { configEnv } from "../environments";
import { OptionsDto } from "../types/options-dto";
import { UserDto } from "../types/user-dto";
import { UpdateUserDto } from "../types/update-user-dto";

export const ApiService = {
  async login(credentials: LoginDto) {
    return await axios.post(`${configEnv.API_URL}/v1/login`, credentials);
  },
  async listAllUser(token: string, options?: OptionsDto) {
    const reqOptions = options ?? { limit: 100, page: 0 };
    const config = {
      headers: {
        "x-access-token": `${token}`,
      },
    };
    return await axios.get(
      `${configEnv.API_URL}/${configEnv.USERS_URL}?limit=${reqOptions.limit}&page=${reqOptions.page}`,
      config
    );
  },
  async createUser(token: string, user: UserDto) {
    const config = {
      headers: {
        "x-access-token": `${token}`,
      },
    };
    return await axios.post(
      `${configEnv.API_URL}/${configEnv.USERS_URL}`,
      user,
      config
    );
  },
  async getOneUser(token: string, id: string) {
    const config = {
      headers: {
        "x-access-token": `${token}`,
      },
    };
    return await axios.get(
      `${configEnv.API_URL}/${configEnv.USERS_URL}/${id}`,
      config
    );
  },
  async updateUser(token: string, id: string, user: UpdateUserDto) {
    const config = {
      headers: {
        "x-access-token": `${token}`,
      },
    };
    return await axios.put(
      `${configEnv.API_URL}/${configEnv.USERS_URL}/${id}`,
      user,
      config
    );
  },
  async deleteUser(token: string, id: string) {
    const config = {
      headers: {
        "x-access-token": `${token}`,
      },
    };
    return await axios.delete(
      `${configEnv.API_URL}/${configEnv.USERS_URL}/${id}`,
      config
    );
  },
};
