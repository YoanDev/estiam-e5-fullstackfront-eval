import Navbar from "../components/organisms/navbar/Navbar";

type Props = {};

const Home = (props: Props) => {
  return (
    <div>
      <Navbar />
    </div>
  );
};

export default Home;
