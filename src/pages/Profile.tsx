import Navbar from "../components/organisms/navbar/Navbar";
import ProfileInfos from "../components/organisms/profile-info/profile-info";
import UserList from "../components/organisms/users/user-list";

type Props = {};

const Profile = (props: Props) => {
  return (
    <>
      <Navbar />
      <ProfileInfos />
    </>
  );
};

export default Profile;
