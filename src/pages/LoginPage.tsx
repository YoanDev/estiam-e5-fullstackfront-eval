import React from "react";
import Navbar from "../components/organisms/navbar/Navbar";
import SignIn from "../components/organisms/SignIn/SignIn";

type Props = {};

const LoginPage = (props: Props) => {
  return (
    <>
      <Navbar />
      <SignIn />
    </>
  );
};

export default LoginPage;
