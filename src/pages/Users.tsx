import Navbar from "../components/organisms/navbar/Navbar";
import UserList from "../components/organisms/users/user-list";

type Props = {};

const Users = (props: Props) => {
  return (
    <>
      <Navbar />
      <UserList />
    </>
  );
};

export default Users;
