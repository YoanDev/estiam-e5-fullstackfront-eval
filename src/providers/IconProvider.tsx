import { IconContext } from "react-icons";

type Props = {
  children: any;
  color?: string;
  className?: string;
};

const IconProvider = ({ children, color = "#000", className }: Props) => {
  return (
    <IconContext.Provider value={{ color, className }}>
      {children}
    </IconContext.Provider>
  );
};

export default IconProvider;
