import React, { useEffect } from "react";
import "./Toaster.scss";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { cleanup } from "@testing-library/react";

type ToasterProps = {
  toastMsg: string;
  id?: string;
  classNameToast?: string;
  error?: boolean;
  info?: boolean;
  warn?: boolean;
  success?: boolean;
  dark?: boolean;
  position?:
    | "top-right"
    | "top-center"
    | "top-left"
    | "bottom-right"
    | "bottom-center"
    | "bottom-left";
};

const Toaster = ({
  toastMsg,
  dark,
  error,
  info,
  success,
  warn,
  classNameToast,
  id,
  position = "bottom-right",
}: ToasterProps) => {
  const notifyErr = () =>
    toast.error(toastMsg, {
      toastId: id,
      className: classNameToast,
      autoClose: 2700,
      position: position,
    });
  const notifyInfo = () =>
    toast.info(toastMsg, {
      toastId: id,
      className: classNameToast,
      autoClose: 2700,
      position: position,
    });
  const notifyWarn = () =>
    toast.warn(toastMsg, {
      toastId: id,
      className: classNameToast,
      autoClose: 2700,
      position: position,
    });
  const notifySuccess = () =>
    toast.success(toastMsg, {
      toastId: id,
      className: classNameToast,
      autoClose: 2700,
      position: position,
    });
  const notifyDark = () =>
    toast.dark(toastMsg, {
      toastId: id,
      className: classNameToast,
      autoClose: 2700,
      position: position,
    });

  useEffect(() => {
    if (error) notifyErr();
    if (info) notifyInfo();
    if (warn) notifyWarn();
    if (success) notifySuccess();
    if (dark) notifyDark();

    const timer = setTimeout(() => {
      cleanup();
    }, 3500);
    return () => clearTimeout(timer);
    // eslint-disable-next-line
  }, [error, info, warn, success, dark]);
  return (
    <>
      <div>
        <ToastContainer limit={3} />
      </div>
    </>
  );
};

export default Toaster;
