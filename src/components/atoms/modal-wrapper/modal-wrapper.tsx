import { ReactNode } from "react";
import styled from "styled-components";
import { IoMdClose } from "react-icons/io";

type Props = { children: ReactNode; closeModal: () => void };

const ModalWrapper = ({ children, closeModal }: Props) => {
  return (
    <StyledModalWrapper>
      {children}
      <StyledCloseIcon onClick={closeModal} />
    </StyledModalWrapper>
  );
};

export default ModalWrapper;

const StyledModalWrapper = styled.div`
  position: fixed;
  display: flex;
  flex-direction: column;
  height: 100%;
  background: rgba(0, 0, 0, 0.619);
  width: 100%;
  align-items: center;
  justify-content: center;
  overflow: hidden;
  left: 0;
  top: 0;
  z-index: 99999;
`;

const StyledCloseIcon = styled(IoMdClose)`
  position: absolute;
  right: 10px;
  top: 20px;
  width: 40px;
  height: 40px;
  color: #fff;
  cursor: pointer;
`;
