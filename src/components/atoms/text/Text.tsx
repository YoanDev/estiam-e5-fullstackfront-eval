import { ReactNode } from "react";
import styled from "styled-components";

export type TextProps = {
  children: string | ReactNode;
  fontSize?: string;
  color?: string;
  padding?: string;
  isBold?: boolean;
  opacity?: string;
  textAlign?: string;
  whiteSpace?: string;
  hoverColor?: string;
};

/**
 * @author Yoan MENDES SEMEDO
 * @property {string | React.ReactNode} children
 * @property {string} fontSize css property
 * @property {string} color css property
 * @property {string} padding css property
 * @property {boolean} isBold css property
 * @property {string} opacity css property
 * @property {string} textAlign css property
 * @property {string} whiteSpace css property
 * @property {string} hoverColor css property
 */
const Text = ({
  color,
  fontSize,
  padding,
  children,
  isBold,
  opacity,
  textAlign,
  whiteSpace,
  hoverColor,
}: TextProps) => {
  return (
    <StyledText
      color={color}
      opacity={opacity}
      fontSize={fontSize}
      padding={padding}
      textAlign={textAlign}
      isBold={isBold}
      whiteSpace={whiteSpace}
      hoverColor={hoverColor}
    >
      {children}
    </StyledText>
  );
};

export default Text;

const StyledText = styled.p<TextProps>`
  font-size: ${(props) => (props.fontSize ? props.fontSize : "16px")};
  color: ${(props) => (props.color ? props.color : "#000")};
  font-weight: ${(props) => props.isBold && "bold"};
  padding: ${(props) => props.padding && props.padding};
  opacity: ${(props) => props.opacity && props.opacity};
  text-align: ${(props) => props.textAlign && props.textAlign};
  white-space: ${(props) => props.whiteSpace && props.whiteSpace};
  height: max-content;
  transition: 0.2s all ease-in-out;

  &:hover {
    color: ${(props) => props.hoverColor};
  }
`;
