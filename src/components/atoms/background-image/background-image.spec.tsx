import { render } from "@testing-library/react";
import BackgroundImage, { BackgroundImageProps } from "./BackgroundImage";
import Image from "../../../images/gold.jpg";

const mockProps: BackgroundImageProps = {
  src: Image,
  alt: "testAlt",
};

describe("BackgroundImage", () => {
  it("BackgroundImage should be defined", () => {
    render(<BackgroundImage {...mockProps} />);
  });
});
