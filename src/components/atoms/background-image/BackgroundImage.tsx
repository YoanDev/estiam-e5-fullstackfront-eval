import styled from "styled-components";

export type BackgroundImageProps = { src: any; alt?: string };

/**
 * @author Yoan MENDES SEMEDO
 * @property {any} src image of the background
 * @property {string} alt of the img
 */
const BackgroundImage = ({ src, alt = "img" }: BackgroundImageProps) => {
  return <StyledImg src={src} alt={alt} />;
};

export default BackgroundImage;

const StyledImg = styled.img`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  opacity: 1;
  filter: brightness(50%);
`;
