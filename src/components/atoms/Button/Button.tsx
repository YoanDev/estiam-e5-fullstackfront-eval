import { ReactNode } from "react";
import styles from "./button.module.scss";

type ButtonProps = {
  children: ReactNode | string;
  handleClick?: any;
  width?: string;
  height?: string;
  background?: string;
  color?: string;
  fontSize?: string;
  border?: string;
  margin?: string;
  type?: "button" | "submit" | "reset";
};

const Button = ({
  children,
  handleClick,
  background,
  width,
  height,
  color,
  fontSize = "16px",
  border,
  type,
  margin,
}: ButtonProps) => {
  return (
    <button
      className={styles.button}
      style={{ width, height, background, color, fontSize, border, margin }}
      onClick={handleClick}
      type={type}
    >
      {children}
    </button>
  );
};

export default Button;
