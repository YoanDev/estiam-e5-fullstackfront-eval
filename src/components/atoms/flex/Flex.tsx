import React from "react";
import styled from "styled-components";

export type FlexProps = {
  children: React.ReactNode;
  alignItems?: "flex-start" | "flex-end" | "center" | "baseline" | "stretch";
  justifyContent?:
    | "flex-start"
    | "flex-end"
    | "center"
    | "space-between"
    | "space-around"
    | "space-evenly";
  flexDirection?: "column" | "row";
  position?: "relative" | "absolute";
  width?: string;
  height?: string;
  padding?: string;
  zIndex?: number;
};

/**
 * @author Yoan MENDES SEMEDO
 * @property {React.ReactNode} children
 * @property {flex-start | flex-end | center | baseline | stretch} alignItems css property
 * @property { flex-start | flex-end | center | space-between | space-around | space-evenly} justifyContent css property
 * @property {row | column} flexDirection css property
 * @property {string} width css property
 * @property {string} height css property
 * @property {string} padding css property
 * @property {relative | absolute} position css property
 * @property {number} zIndex css property
 */
const Flex = ({
  children,
  alignItems,
  justifyContent,
  flexDirection,
  width,
  height,
  padding,
  zIndex,
  position,
}: FlexProps) => {
  return (
    <StyledFlex
      alignItems={alignItems}
      justifyContent={justifyContent}
      flexDirection={flexDirection}
      width={width}
      height={height}
      padding={padding}
      zIndex={zIndex}
      position={position}
    >
      {children}
    </StyledFlex>
  );
};

export default Flex;

const StyledFlex = styled.div<FlexProps>`
  display: flex;
  align-items: ${(props) => props.alignItems && props.alignItems};
  justify-content: ${(props) => props.justifyContent && props.justifyContent};
  flex-direction: ${(props) => props.flexDirection && props.flexDirection};
  width: ${(props) => props.width && props.width};
  height: ${(props) => props.height && props.height};
  padding: ${(props) => props.padding && props.padding};
  z-index: ${(props) => props.zIndex && props.zIndex};
  position: ${(props) => props.position && props.position};
`;
