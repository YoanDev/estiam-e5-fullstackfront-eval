import { render } from "@testing-library/react";
import Spacer, { SpacerProps } from "./Spacer";
import Image from "../../../images/gold.jpg";

const mockProps: SpacerProps = {
  width: "20px",
  height: "20px",
};

describe("Spacer", () => {
  it("Spacer should be defined", () => {
    render(<Spacer {...mockProps} />);
  });
});
