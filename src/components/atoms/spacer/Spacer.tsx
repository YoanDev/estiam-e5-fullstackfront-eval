import styled from "styled-components";

export type SpacerProps = {
  width?: string;
  height?: string;
};

/**
 * @author Yoan MENDES SEMEDO
 * @property {string} width css property
 * @property {string} height css property
 */
const Spacer = ({ width, height }: SpacerProps) => {
  return <StyledSpacer width={width} height={height} />;
};

export default Spacer;

const StyledSpacer = styled.div<SpacerProps>`
  width: ${(props) => props.width && props.width};
  height: ${(props) => props.height && props.height};
`;
