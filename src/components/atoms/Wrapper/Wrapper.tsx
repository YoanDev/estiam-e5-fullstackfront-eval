import React, { ReactNode } from "react";
import styled from "styled-components";

type Props = {
  children: ReactNode;
  padding?: string;
};

const Wrapper = ({ padding, children }: Props) => {
  return <StyledWrapper padding={padding}>{children}</StyledWrapper>;
};

export default Wrapper;

const StyledWrapper = styled.div<Props>`
  position: relative;
  display: flex;
  flex-direction: column;
  padding: ${(props) => props.padding ?? "10px"};
`;
