import { Field, Form } from "react-final-form";
import styled from "styled-components";
import { UserDto } from "../../../types/user-dto";
import Button from "../../atoms/Button/Button";
import Flex from "../../atoms/flex/Flex";
import ModalWrapper from "../../atoms/modal-wrapper/modal-wrapper";
import Spacer from "../../atoms/spacer/Spacer";
import Text from "../../atoms/text/Text";

type Props = {
  closeModal: () => void;
  createUser: (user: UserDto) => Promise<void>;
};

const UserCreateForm = ({ closeModal, createUser }: Props) => {
  const onSubmit = (formValues: UserDto) => {
    createUser(formValues);
    closeModal();
  };
  return (
    <ModalWrapper closeModal={closeModal}>
      <Form
        onSubmit={onSubmit}
        render={({ handleSubmit }) => (
          <StyledForm onSubmit={handleSubmit}>
            <Flex
              zIndex={20}
              flexDirection="column"
              width="200px"
              justifyContent="center"
            >
              <Flex>
                <Text isBold fontSize="20px" color="#fff">
                  Création d'utilisateur
                </Text>
              </Flex>
              <Spacer height="20px" />
              <Flex width="100%">
                <Text color="#fff">Prénom</Text>
              </Flex>
              <Field name="firstName">
                {(props) => (
                  <div>
                    <StyledField {...props.input} />
                  </div>
                )}
              </Field>
              <Spacer height="20px" />
              <Flex width="100%">
                <Text color="#fff">Nom de famille</Text>
              </Flex>
              <Field name="lastName">
                {(props) => (
                  <div>
                    <StyledField {...props.input} />
                  </div>
                )}
              </Field>
              <Spacer height="20px" />

              <Flex width="100%">
                <Text color="#fff">Email</Text>
              </Flex>
              <Field name="email">
                {(props) => (
                  <div>
                    <StyledField {...props.input} />
                  </div>
                )}
              </Field>
              <Spacer height="20px" />

              <Flex width="100%">
                <Text color="#fff">Mot de passe</Text>
              </Flex>
              <Field name="password">
                {(props) => (
                  <div>
                    <StyledField type={"password"} {...props.input} />
                  </div>
                )}
              </Field>
              <Spacer height="20px" />
              <Button>Confirmer</Button>
            </Flex>
          </StyledForm>
        )}
      ></Form>
    </ModalWrapper>
  );
};

export default UserCreateForm;

const StyledField = styled.input`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  padding: 4px;
  border-radius: 8px;
`;

const StyledForm = styled.form`
  position: relative;
  display: flex;
  flex-direction: column;
  min-height: 200px;
  background: #000;
  width: 400px;
  align-items: center;
  justify-content: center;
  border-radius: 20px;
  overflow: hidden;
  padding: 20px;
`;
