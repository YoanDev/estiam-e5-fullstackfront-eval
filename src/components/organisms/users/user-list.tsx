import React, { useEffect, useState } from "react";
import styled from "styled-components";
import useHandleUsers from "../../../hooks/useHandleUsers";
import Button from "../../atoms/Button/Button";
import Flex from "../../atoms/flex/Flex";
import Spacer from "../../atoms/spacer/Spacer";
import Text from "../../atoms/text/Text";
import Wrapper from "../../atoms/Wrapper/Wrapper";
import UserCreateForm from "./user-create-form";
import UserItem from "./user-item";

const UserList = () => {
  const [createUserModal, setCreateUserModal] = useState(false);
  const {
    getListUsers,
    userList,
    nextPage,
    prevPage,
    page,
    maxPage,
    updateUser,
    deleteUser,
    createUser,
  } = useHandleUsers();

  useEffect(() => {
    getListUsers();
  }, [page, nextPage, prevPage, getListUsers]);

  const openCreateUserModal = () => {
    setCreateUserModal(true);
  };
  const closeCreateUserModal = () => {
    setCreateUserModal(false);
  };

  return (
    <Wrapper padding="20px">
      <Button handleClick={openCreateUserModal}>Ajouter utilisateur</Button>
      <Spacer height="20px" />
      {createUserModal && (
        <UserCreateForm
          closeModal={closeCreateUserModal}
          createUser={createUser}
        />
      )}
      <Flex width="100%" alignItems="center" justifyContent="center">
        <Button handleClick={prevPage}>Précédent</Button>
        <Spacer width="20px" />
        <Text color="#fff">{page}</Text>
        <Text color="#fff">/</Text>
        <Text color="#fff">{maxPage}</Text>
        <Spacer width="20px" />
        <Button handleClick={nextPage}>Suivant</Button>
      </Flex>
      <StyledUserList>
        {userList?.users.map((user) => (
          <UserItem
            key={user.id}
            user={user}
            updateUser={updateUser}
            deleteUser={deleteUser}
          />
        ))}
      </StyledUserList>
      <Flex width="100%" alignItems="center" justifyContent="center">
        <Button handleClick={prevPage}>Précédent</Button>
        <Spacer width="20px" />
        <Text color="#fff">{page}</Text>
        <Text color="#fff">/</Text>
        <Text color="#fff">{maxPage}</Text>
        <Spacer width="20px" />
        <Button handleClick={nextPage}>Suivant</Button>
      </Flex>
    </Wrapper>
  );
};

export default UserList;

export const StyledUserList = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  gap: 20px;
  padding: 20px;
  width: 100%;

  @media screen and (min-width: 570px) {
    grid-template-columns: 1fr 1fr;
  }

  @media screen and (min-width: 780px) {
    grid-template-columns: 1fr 1fr 1fr;
  }
`;
