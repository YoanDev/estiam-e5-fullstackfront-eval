import { useState } from "react";
import styled from "styled-components";
import { UpdateUserDto } from "../../../types/update-user-dto";
import { User } from "../../../types/user";
import Button from "../../atoms/Button/Button";
import Flex from "../../atoms/flex/Flex";
import Spacer from "../../atoms/spacer/Spacer";
import Text from "../../atoms/text/Text";
import UserUpdateForm from "./user-update-form";

interface Props {
  user: User;
  updateUser: (id: string, user: UpdateUserDto) => Promise<void>;
  deleteUser: (id: string) => Promise<void>;
}

const UserItem = ({ user, deleteUser, updateUser }: Props) => {
  const [updateModal, setUpdateModal] = useState(false);

  const onSubmit = async (formValues: UpdateUserDto) => {
    updateUser(user.id, formValues);
    setUpdateModal(false);
  };

  const closeModal = () => {
    setUpdateModal(false);
  };
  const openUpdateModal = () => {
    setUpdateModal(true);
  };
  return (
    <StyledUserItem>
      <Text isBold fontSize="20px">
        Id
      </Text>
      <Text>{user.id}</Text>
      <Spacer height="8px" />
      <Text isBold fontSize="20px">
        Prénom
      </Text>
      <Text>{user.firstName}</Text>

      <Spacer height="8px" />
      <Text isBold fontSize="20px">
        Nom de famille
      </Text>
      <Text>{user.lastName}</Text>
      <Spacer height="8px" />

      <Text isBold fontSize="20px">
        Email
      </Text>
      <Text>{user.email}</Text>
      <Spacer height="8px" />
      <Text isBold fontSize="20px">
        Ville
      </Text>
      <Text>{user.city}</Text>
      <Spacer height="8px" />

      <Text isBold fontSize="20px">
        Téléphone
      </Text>
      <Text>{user.phoneNumber}</Text>
      <Spacer height="20px" />
      <Flex width="100%" alignItems="center" justifyContent="center">
        <Button handleClick={openUpdateModal}>Modifier</Button>

        <Spacer width="20px" />
        <Button
          handleClick={() => {
            deleteUser(user.id);
          }}
        >
          Supprimer
        </Button>
      </Flex>
      {updateModal && (
        <UserUpdateForm
          closeModal={closeModal}
          onSubmit={onSubmit}
          user={user}
        />
      )}
    </StyledUserItem>
  );
};

export default UserItem;

export const StyledUserItem = styled.div`
  display: flex;
  position: relative;
  flex-direction: column;
  padding: 20px;
  width: 100%;
  background: #fff;
  height: 400px;
  border-radius: 8px;
  &:before {
    content: "";
    background: #4d25ff44;
    position: absolute;
    left: 0;
    top: 0;
    width: 100%;
    height: 100%;
    clip-path: polygon(100% 0%, 100% 100%, 85% 100%, 85% 0%);
  }
  &:nth-child(2n) {
    &:before {
      content: "";
      background: #ff25db44;
      position: absolute;
      left: 0;
      top: 0;
      width: 100%;
      height: 100%;
      clip-path: polygon(100% 0%, 100% 100%, 85% 100%, 85% 0%);
    }
  }
`;
