import { useContext, useEffect } from "react";
import useHandleUsers from "../../../hooks/useHandleUsers";
import { AuthContext } from "../../../providers/AuthContextProvider";
import Wrapper from "../../atoms/Wrapper/Wrapper";
import UserItem from "../users/user-item";

type Props = {};

const ProfileInfos = (props: Props) => {
  const authContext = useContext(AuthContext);
  const { getUser, user, deleteUser, updateUser } = useHandleUsers();

  const currentUser = authContext.state.currentUser;

  useEffect(() => {
    getUser(currentUser.id);
  }, []);

  return (
    <Wrapper padding="80px">
      {user && (
        <UserItem user={user} deleteUser={deleteUser} updateUser={updateUser} />
      )}
    </Wrapper>
  );
};

export default ProfileInfos;
