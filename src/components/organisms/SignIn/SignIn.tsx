import { Form, Field } from "react-final-form";
import styled from "styled-components";
import BackgroundImage from "../../atoms/background-image/BackgroundImage";
import Flex from "../../atoms/flex/Flex";
import Spacer from "../../atoms/spacer/Spacer";
import Text from "../../atoms/text/Text";
import Background1 from "../../../assets/images/bg4.jpg";
import Wrapper from "../../atoms/Wrapper/Wrapper";
import Button from "../../atoms/Button/Button";
import useHandleUsers from "../../../hooks/useHandleUsers";

type Props = {};

const SignIn = (props: Props) => {
  const { loginApp } = useHandleUsers();

  return (
    <Wrapper padding="20px 100px">
      <Form
        onSubmit={loginApp}
        render={({ handleSubmit }) => (
          <StyledForm onSubmit={handleSubmit}>
            <BackgroundImage src={Background1} />
            <Flex
              zIndex={20}
              flexDirection="column"
              width="200px"
              justifyContent="center"
            >
              <Flex justifyContent="center" width="100%">
                <Text isBold fontSize="30px" color="#fff">
                  Login
                </Text>
              </Flex>
              <Spacer height="20px" />
              <Flex width="100%">
                <Text color="#fff">Email</Text>
              </Flex>
              <Field name="email">
                {(props) => (
                  <div>
                    <StyledField {...props.input} type="email" />
                  </div>
                )}
              </Field>
              <Spacer height="20px" />
              <Flex width="100%">
                <Text color="#fff">Password</Text>
              </Flex>
              <Field name="password">
                {(props) => (
                  <div>
                    <StyledField {...props.input} type="password" />
                  </div>
                )}
              </Field>
              <Spacer height="20px" />
              <Flex width="100%" justifyContent="center">
                <Button>Se connecter</Button>
              </Flex>
            </Flex>
          </StyledForm>
        )}
      />
    </Wrapper>
  );
};

export default SignIn;

const StyledForm = styled.form`
  position: relative;
  display: flex;
  flex-direction: column;
  background: #000;
  opacity: 0.8;
  width: 100%;
  min-height: 300px;
  align-items: center;
  justify-content: center;
  border-radius: 20px;
  overflow: hidden;
`;

const StyledField = styled.input`
  display: flex;
  flex-direction: column;
  width: 100%;
  align-items: center;
  padding: 4px;
  border-radius: 8px;
`;
