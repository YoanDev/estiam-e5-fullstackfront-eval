import styled from "styled-components";
import Flex from "../../atoms/flex/Flex";
import Text from "../../atoms/text/Text";
import { NavLink, useNavigate } from "react-router-dom";
import { AuthContext } from "../../../providers/AuthContextProvider";
import { useContext } from "react";
import Button from "../../atoms/Button/Button";

const Navbar = () => {
  const authContext = useContext(AuthContext);
  const navigate = useNavigate();

  const logout = () => {
    authContext.state.logout();
    authContext.dispatch({ type: "LOGOUT" });
    navigate("/");
  };
  return (
    <StyledNavbar>
      <Flex justifyContent="flex-end" width="100%" alignItems="center">
        <NavLink to="/">
          <Text color="#FFF" hoverColor="#E39710" padding="0 20px">
            Accueil
          </Text>
        </NavLink>

        {!authContext.state.isLoggedIn && (
          <NavLink to="/authentication">
            <Text color="#FFF" hoverColor="#E39710" padding="0 20px">
              Authentification
            </Text>
          </NavLink>
        )}
        {authContext.state.isLoggedIn && (
          <NavLink to="/users">
            <Text color="#FFF" hoverColor="#E39710" padding="0 20px">
              Utilisateurs
            </Text>
          </NavLink>
        )}
        {authContext.state.isLoggedIn && (
          <NavLink to="/profile">
            <Text color="#FFF" hoverColor="#E39710" padding="0 20px">
              Profil
            </Text>
          </NavLink>
        )}
        {authContext.state.isLoggedIn && (
          <Button handleClick={logout}>Déconnexion</Button>
        )}
      </Flex>
    </StyledNavbar>
  );
};

export default Navbar;

const StyledNavbar = styled.div`
  z-index: 999;
  position: relative;
  display: flex;
  height: 80px;
  align-items: center;
  padding: 0 20px;
  width: 100%;
  box-shadow: rgba(149, 157, 165, 0.2) 0px 8px 24px;
  p {
    padding: 0 10px;
    font-size: 9px;
  }
  @media screen and (min-width: 320px) {
    p {
      font-size: 12px;
    }
  }
  @media screen and (min-width: 490px) {
    p {
      font-size: 16px;
    }
  }
`;
