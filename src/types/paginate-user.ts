import { User } from "./user";

export interface PaginateUser {
  pageNumber: number;
  usersPerPage: number;
  usersCount: number;
  users: User[];
}
