export type UserDto = {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
};
