export type OptionsDto = {
  limit?: number;
  page?: number;
};
