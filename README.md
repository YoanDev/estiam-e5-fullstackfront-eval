# Démarrer le projet

npm i
npm start

# Groupe

- Yoan MENDES SEMEDO
- Amine ABBES

# Fonctionnalités

## utilisateur non authentifié

- Authentification

## utilisateur authentifié

- Création d'utilisateur
- Liste paginée des utilisateurs
- Suppression d'utilisateur
- Modification du prénom d'un utilisateur
- Récupération des données de l'utilisateur connecté dans le profil
- Modification du prénom dans les données du profil de l'utilisateur connecté
- Déconnexion

# Architecture

Atomic Design

https://blog-ux.com/quest-ce-que-latomic-design/
